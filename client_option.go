package client_slack_claude

import "time"

type Option struct {
	AuthToken       string
	BotID           string
	Proxy           string
	PollingInterval time.Duration
	MaxPollingTime  int
}

func (o *Option) validate() error {
	if o.AuthToken == "" || o.BotID == "" {
		return ErrBadRequest
	}
	return nil
}

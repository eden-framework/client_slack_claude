package client_slack_claude

import "fmt"

func Key(k string) string {
	return fmt.Sprintf("${%s}", k)
}

package client_slack_claude

import (
	"context"
	"github.com/sirupsen/logrus"
	"github.com/slack-go/slack"
	"strings"
	"time"
)

func (c *clientSlackClaude) CreateChatCompletionByTemplate(
	ctx context.Context,
	template string,
	vars map[string]string,
) (
	result []string,
	err error,
) {
	prompt := template
	for k, v := range vars {
		prompt = strings.ReplaceAll(prompt, Key(k), v)
	}
	return c.CreateChatCompletion(ctx, prompt)
}

func (c *clientSlackClaude) CreateChatCompletion(ctx context.Context, prompt string) (
	result []string,
	err error,
) {
	logrus.Debugf("[clientSlackClaude] CreateChatCompletion prompt: %s", prompt)
	_, timestamp, err := c.client.PostMessageContext(ctx, c.channelID, slack.MsgOptionText(prompt, false))
	if err != nil {
		logrus.Errorf("[clientSlackClaude] CreateChatCompletion: s.client.PostMessageContext error: %v", err)
		return nil, err
	}
	ticker := time.NewTicker(c.opt.PollingInterval)
	pollingTime := 0
	for {
		select {
		case <-ticker.C:
			history, err := c.client.GetConversationHistoryContext(
				ctx,
				&slack.GetConversationHistoryParameters{
					ChannelID: c.channelID,
					Oldest:    timestamp,
				},
			)
			if err == nil {
				for i := len(history.Messages) - 1; i >= 0; i-- {
					if history.Messages[i].User == c.opt.BotID && !strings.HasSuffix(
						history.Messages[i].Text,
						"Typing…_",
					) {
						result = append(result, history.Messages[i].Text)
						return result, nil
					}
				}
			}
			pollingTime++
			if pollingTime >= c.opt.MaxPollingTime {
				return nil, ErrUpstreamRequestFailed
			}
		case <-ctx.Done():
			return nil, ctx.Err()
		}
	}
	return
}

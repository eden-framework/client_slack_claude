package client_slack_claude

import (
	"crypto/tls"
	"github.com/sirupsen/logrus"
	"github.com/slack-go/slack"
	"net/http"
	"net/url"
	"sync"
)

var instance *clientSlackClaude
var once sync.Once

type clientSlackClaude struct {
	opt       Option
	client    *slack.Client
	channelID string
}

func GetClient() *clientSlackClaude {
	once.Do(
		func() {
			instance = &clientSlackClaude{}
		},
	)
	return instance
}

func (c *clientSlackClaude) Init(option any) {
	opt, ok := option.(Option)
	if !ok {
		logrus.Fatalf("[clientSlackClaude] Init failed: option is not Option type")
	}

	c.opt = opt

	if err := c.opt.validate(); err != nil {
		logrus.Panicf("[clientSlackClaude] Init failed: err=%v", err)
	}

	if c.opt.Proxy != "" {
		proxy, err := url.Parse(c.opt.Proxy)
		if err != nil {
			logrus.Panicf("[clientOpenAI] Init failed: err=%v", err)
		}
		transport := &http.Transport{
			Proxy:           http.ProxyURL(proxy),
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		httpClient := &http.Client{Transport: transport}
		c.client = slack.New(c.opt.AuthToken, slack.OptionHTTPClient(httpClient))
	} else {
		c.client = slack.New(c.opt.AuthToken)
	}

	channel, _, _, err := c.client.OpenConversation(
		&slack.OpenConversationParameters{
			Users: []string{c.opt.BotID},
		},
	)
	if err != nil {
		logrus.Errorf("[clientSlackClaude] Init failed: c.client.OpenConversation err=%v", err)
		return
	}
	c.channelID = channel.ID
}

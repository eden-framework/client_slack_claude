package client_slack_claude

import "errors"

var (
	ErrBadRequest            = errors.New("bad request")
	ErrUpstreamRequestFailed = errors.New("upstream request failed")
)
